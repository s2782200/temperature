public class TempFormulas {
    static Double cToF(String celcius) {
        try {
            return (Double.parseDouble(celcius) * (9.0/5.0)) + 32;
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
