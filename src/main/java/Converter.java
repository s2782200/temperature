import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Converter extends HttpServlet {
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celsius to Fahrenheit Converter";

        Double temp = TempFormulas.cToF(request.getParameter("temp"));

        if (temp != null) {
            out.println(docType +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    "  <P>Temperature in °C: " +
                    request.getParameter("temp") + "\n" +
                    "  <P>Temperature in °F: " +
                    temp +
                    "</BODY></HTML>");
        } else {
            out.println(docType +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    " <H2>Invalid Input</H2>" +
                    "</BODY></HTML>");
        }
    }
}
